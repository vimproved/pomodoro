use clap::Parser;
use std::io::{stdin, stdout, Write};
use std::thread::sleep;
use std::time;

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct Args {
    #[arg(short, long, default_value_t = 25, value_name = "MINUTES")]
    work: i32,

    #[arg(short, long = "break", default_value_t = 5, value_name = "MINUTES")]
    breaks: i32,
}

fn main() {
    let args = Args::parse();
    'main: loop {
        println!("Starting {} minute timer for work!", args.work);
        timer(args.work * 60);
        println!("\nWork time up!");
        notifica::notify(
            "Pomodoro",
            "Work time is over! Beginning 5 minutes of break.",
        )
        .unwrap();
        println!("Starting {} minute timer for break!", args.breaks);
        timer(args.breaks * 60);
        notifica::notify("Pomodoro", "Break time is over. Please confirm whether you would like to restart work time in the terminal.")
            .unwrap();
        print!("\nBreak time up! Would you like to restart work time? [Y/n] ");
        stdout().flush().unwrap();
        'input: loop {
            let mut user_input = String::new();
            stdin().read_line(&mut user_input).unwrap();
            match { user_input.as_str() } {
                "Y\n" | "\n" => {
                    println!("Restarting work timer!");
                    break 'input;
                }
                "n\n" => {
                    println!("Okay, stopping program.");
                    break 'main;
                }
                _ => println!("Please input Y or n!"),
            }
        }
    }
}

fn timer(seconds: i32) {
    let mut elapsed = 0;
    'timer: loop {
        if elapsed > seconds {
            break 'timer;
        }
        let mins = (seconds - elapsed) / 60;
        let secs: i32 = (seconds - elapsed) % 60;
        print!("\rTime remaining: {:.0}:{:02}", mins, secs);
        stdout().flush().unwrap();
        sleep(time::Duration::from_secs(1));
        elapsed += 1;
    }
}
